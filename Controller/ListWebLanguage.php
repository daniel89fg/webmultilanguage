<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Controller;

use FacturaScripts\Core\Lib\ExtendedController;

/**
 * Description of ListLanguage
 *
 * @author Athos Online <info@athosonline.com>
 */
class ListWebLanguage extends ExtendedController\ListController
{
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $data = parent::getPageData();
        $data['menu'] = 'admin';
        $data['title'] = 'languages';
        $data['icon'] = 'fas fa-language';
        $data['showonmenu'] = false;
        return $data;
    }
    
    protected function createViews() {
        $this->createViewsLanguages();
    }
    
    /**
     * 
     * @param string $viewName
     */
    protected function createViewsLanguages(string $viewName = 'ListWebLanguage')
    {
        $this->addView($viewName, 'WebLanguage', 'languages', 'fas fa-language');
    }
}