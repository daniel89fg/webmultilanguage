<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Controller;

use FacturaScripts\Core\Lib\ExtendedController\EditController;
use FacturaScripts\Dinamic\Model\WebTranslate;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of EditWebTranslate
 *
 * @author Athos Online <info@athosonline.com>
 */
class EditWebTranslate extends EditController
{
    /**
     * Returns the model name
     *
     * @return string
     */
    public function getModelClassName()
    {
        return 'WebTranslate';
    }

    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['menu'] = 'web';
        $pagedata['title'] = 'translate';
        $pagedata['icon'] = 'fas fa-language';
        $data['showonmenu'] = false;
        return $pagedata;
    }

    /**
     * 
     * @param string $action
     *
     * @return bool
     */
    protected function execPreviousAction($action)
    {
        switch ($action) {
            case 'insert':
                $this->createCustomTranslate();
                $trans = new WebTranslate();
                $where = [
                    new DataBaseWhere('codicu', $this->request->request->get('codicu')),
                    new DataBaseWhere('keytrans', $this->request->request->get('keytrans')),
                    new DataBaseWhere('modelid', null),
                    new DataBaseWhere('modelname', null)
                ];
                $trans->loadFromCode('', $where);

                if (!isset($trans->idtrans)) {
                    if (parent::execPreviousAction($action)) {
                        $this->writeCustomTranslate();
                        return true;
                    }
                } else {
                    $this->toolBox()->i18nLog()->error('duplicate-key-translate', ['%key%' => $this->request->request->get('keytrans'), '%lang%' => $this->request->request->get('codicu')]);
                    return false;
                }
                break;
            case 'edit':
                if (parent::execPreviousAction($action)) {
                    $this->writeCustomTranslate();
                    return true;
                }
                break;
            case 'delete':
                if (parent::execPreviousAction($action)) {
                    $this->deleteCustomTranslate();
                    return true;
                }
                break;
        }

        //return parent::execPreviousAction($action);
    }

    protected function writeCustomTranslate()
    {
        $myFile = FS_FOLDER . '/MyFiles/Translation/' . $this->request->request->get('codicu') . '.json';
        $jsonString = file_get_contents($myFile);
        $data = json_decode($jsonString, true);
        $data[$this->request->request->get('keytrans')] = $this->request->request->get('valuetrans');
        $json = json_encode($data);
        file_put_contents($myFile, $json);
    }

    protected function deleteCustomTranslate()
    {
        $myFile = FS_FOLDER . '/MyFiles/Translation/' . $this->request->request->get('codicu') . '.json';
        $jsonString = file_get_contents($myFile);
        $data = json_decode($jsonString, true);
        unset($data[$this->request->request->get('keytrans')]);
        $json = json_encode($data);
        file_put_contents($myFile, $json);
    }

    protected function createCustomTranslate()
    {
        $dir = FS_FOLDER . DIRECTORY_SEPARATOR . 'MyFiles/Translation/';
        if (!is_dir($dir)) {
            @mkdir($dir, 0777, true);
        }

        $myFile = FS_FOLDER . '/MyFiles/Translation/' . $this->request->request->get('codicu') . '.json';
        if (!file_exists($myFile)) {
            $json = json_encode(array());
            file_put_contents($myFile, $json);
        }
    }
}