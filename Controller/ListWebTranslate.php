<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Controller;

use FacturaScripts\Core\Lib\ExtendedController;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\WebTranslate;

/**
 * Description of ListWebTranslate
 *
 * @author Athos Online <info@athosonline.com>
 */
class ListWebTranslate extends ExtendedController\ListController
{
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $data = parent::getPageData();
        $data['menu'] = 'web';
        $data['title'] = 'translates';
        $data['icon'] = 'fas fa-language';
        return $data;
    }
    
    protected function createViews() {
        $this->createViewsTranslates();
    }
    
    /**
     * 
     * @param string $viewName
     */
    protected function createViewsTranslates(string $viewName = 'ListWebTranslate')
    {
        $this->addView($viewName, 'WebTranslate', 'translates', 'fas fa-language');
    }

    protected function loadData($viewName, $view) {
        switch ($viewName) {
            case 'ListWebTranslate':
                $where = [
                    new DataBaseWhere('modelid', null),
                    new DataBaseWhere('modelname', null)
                ];
                $view->loadData('', $where);
                break;
        }
    }

    /**
     * 
     * @param string $action
     *
     * @return bool
     */
    protected function execPreviousAction($action)
    {
        switch ($action) {
            case 'delete':
                $this->deleteCustomTranslate();
                break;
        }

        return parent::execPreviousAction($action);
    }

    protected function deleteCustomTranslate()
    {
        $translate = new WebTranslate();
        foreach ($this->request->request->get('code') as $code) {
            $translate->clear();
            $translate->loadFromCode($code);
            $myFile = FS_FOLDER . '/MyFiles/Translation/' . $translate->codicu . '.json';
            $jsonString = file_get_contents($myFile);
            $data = json_decode($jsonString, true);
            unset($data[$translate->keytrans]);
            $json = json_encode($data);
            file_put_contents($myFile, $json);
        }
    }
}