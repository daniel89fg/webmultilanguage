<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Controller;

use FacturaScripts\Core\Base\Controller;
use FacturaScripts\Core\Base\ControllerPermissions;
use FacturaScripts\Dinamic\Model\User;
use FacturaScripts\Dinamic\Model\WebTranslate;
use FacturaScripts\Dinamic\Model\WebLanguage;
use FacturaScripts\Dinamic\Model\WebPage;
use Symfony\Component\HttpFoundation\Response;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of Sitemap
 *
 * @author Athos Online <info@athosonline.com>
 */
class Sitemap extends Controller
{

    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pageData = parent::getPageData();
        $pageData['menu'] = 'web';
        $pageData['title'] = 'sitemap';
        $pageData['showonmenu'] = false;
        return $pageData;
    }

    /**
     * Execute the public part of the controller.
     *
     * @param Response $response
     */
    public function publicCore(&$response)
    {
        parent::publicCore($response);
        $this->generateSitemap();
    }

    /**
     * Runs the controller's private logic.
     *
     * @param Response              $response
     * @param User                  $user
     * @param ControllerPermissions $permissions
     */
    public function privateCore(&$response, $user, $permissions)
    {
        parent::privateCore($response, $user, $permissions);
        $this->generateSitemap();
    }

    /**
     * Returns a valid sitemap item.
     *
     * @param string $loc
     * @param int    $lastmod
     * @param string $changefreq
     * @param float  $priority
     * 
     * @return array
     */
    protected function createItem(string $loc, int $lastmod, string $changefreq = 'weekly', float $priority = 0.5, int $idpage, string $type): array
    {
        return [
            'loc' => $loc,
            'lastmod' => \date('Y-m-d', $lastmod),
            'changefreq' => $changefreq,
            'priority' => $priority,
            'idpage' => $idpage,
            'type' => $type
        ];
    }

    protected function createItemChild(string $codicu, string $loc): array
    {
        return [
            'codicu' => $codicu,
            'loc' => $loc
        ];
    }

    /**
     * Generate sitemap.
     */
    private function generateSitemap()
    {
        $this->setTemplate(false);
        $this->response->headers->set('Content-type', 'text/xml');

        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n"
            . '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.w3.org/TR/xhtml11/xhtml11_schema.html http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/TR/xhtml11/xhtml11_schema.html">' . "\n";
        foreach ($this->getSitemapItems() as $item) {
            $xml .= '<url><loc>' . $item['loc'] . '</loc>';

            foreach ($this->getSitemapItemsChild($item['idpage'], $item['type']) as $itemChild) {
                $xml .= '<xhtml:link'
                    . ' rel="alternate"'
                    . ' hreflang="'.$itemChild['codicu'].'"'
                    . ' href="'.$itemChild['loc'].'" />';
            }

            $xml .= '<lastmod>' . $item['lastmod'] . '</lastmod>'
                . '<changefreq>' . $item['changefreq'] . '</changefreq>'
                . '<priority>' . $item['priority'] . '</priority>'
                . '</url>' . "\n";
        }
        $xml .= '</urlset>';
        
        $this->response->setContent($xml);
    }

    /**
     * Return sitemap items.
     *
     * @return array
     */
    protected function getSitemapItems(): array
    {
        $items = [];
        $webpageModel = new WebPage();
        $translate = new WebTranslate();
        $langDefault = WebLanguage::getWebLanguageDefault();

        foreach ($webpageModel->all([], [], 0, 0) as $wpage) {
            if (isset($wpage->noindex) || \substr($wpage->permalink, -1) === '*') {
                continue;
            }

            $translate->clear();
            $where = [
                new DataBaseWhere('keytrans', 'permalink'),
                new DataBaseWhere('modelid', $wpage->idpage),
                new DataBaseWhere('modelname', $wpage->type),
                new DataBaseWhere('codicu', $langDefault->codicu)
            ];
            $translate->loadFromCode('', $where);

            $items[] = $this->createItem(
                $this->toolBox()->appSettings()->get('webcreator', 'siteurl') . $translate->valuetrans,
                \strtotime($wpage->lastmod),
                'weekly',
                0.8,
                $wpage->idpage,
                $wpage->type
            );
        }

        return $items;
    }
    
    protected function getSitemapItemsChild($idpage, $type): array
    {
        $items = [];
        $translate = new WebTranslate();
        $where = [
            new DataBaseWhere('keytrans', 'permalink'),
            new DataBaseWhere('modelid', $idpage),
            new DataBaseWhere('modelname', $type)
        ];
        foreach ($translate->all($where, [], 0, 0) as $trans) {
            $items[] = $this->createItemChild(
                $trans->codicu,
                $this->toolBox()->appSettings()->get('webcreator', 'siteurl') . $trans->valuetrans
            );
        }
        
        return $items;
    }
}