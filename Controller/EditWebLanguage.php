<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Controller;

use FacturaScripts\Core\Lib\ExtendedController\EditController;
use FacturaScripts\Dinamic\Model\WebLanguage;

/**
 * Description of EditLanguage
 *
 * @author Athos Online <info@athosonline.com>
 */
class EditWebLanguage extends EditController
{
    /**
     * Returns the model name
     *
     * @return string
     */
    public function getModelClassName()
    {
        return 'WebLanguage';
    }

    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['menu'] = 'admin';
        $pagedata['title'] = 'language';
        $pagedata['icon'] = 'fas fa-language';
        return $pagedata;
    }

    /**
     * 
     * @param string $action
     *
     * @return bool
     */
    protected function execAfterAction($action)
    {
        $uploadFile = $this->request->files->get('flag');
        if ($uploadFile) {
            $origDir = FS_FOLDER . DIRECTORY_SEPARATOR . 'MyFiles/';
            $publicDir = FS_FOLDER . DIRECTORY_SEPARATOR . 'MyFiles/Public/';
            if (!is_dir($publicDir)) {
                @mkdir($publicDir, 0777, true);
            }

            copy($origDir . $uploadFile->getClientOriginalName(), $publicDir . $uploadFile->getClientOriginalName());
            unlink($origDir . $uploadFile->getClientOriginalName());
        }

        return parent::execAfterAction($action);
    }

    /**
     * 
     * @param string $action
     *
     * @return bool
     */
    protected function execPreviousAction($action)
    {
        if ($action === 'delete') {
            $language = new WebLanguage();
            $language->loadFromCode($this->request->request->get('code'));
            $publicDir = FS_FOLDER . DIRECTORY_SEPARATOR . 'MyFiles/Public/';
            unlink($publicDir . $language->flag);
        }

        if ($action === 'edit') {
            $language = new WebLanguage();
            $language->loadFromCode($this->request->request->get('code'));
            $uploadFile = $this->request->files->get('flag');

            if ($uploadFile && !is_null($language->flag) && $language->flag != $uploadFile->getClientOriginalName()) {
                $publicDir = FS_FOLDER . DIRECTORY_SEPARATOR . 'MyFiles/Public/';
                unlink($publicDir . $language->flag);
            }
        }

        return parent::execPreviousAction($action);
    }
}