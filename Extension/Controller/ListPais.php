<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Extension\Controller;

class ListPais
{
    public function createViews() {
        return function() {
            $this->addView('ListWebLanguage', 'WebLanguage', 'languages', 'fa fa-language');
        };
    }
}