<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Extension\Controller;

use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\WebLanguage;
use FacturaScripts\Dinamic\Model\Settings;

class WebCreator
{
    public function createViews() {
        return function() {
            if (count(Settings::$fieldsTranslate) > 0) {
                $this->addEditListView('EditSettingsTranslate', 'WebTranslate', 'translations', 'fas fa-edit');
                $this->views['WebSettingsDefault']->disableColumn('lang-code', true);
            }
        };
    }

    protected function loadData() {
        return function($viewName, $view) {
            switch ($viewName) {
                case 'EditSettingsTranslate':
                    // Languages
                    $columnLanguages = $this->views['EditSettingsTranslate']->columnForName('language');
                    if ($columnLanguages && $columnLanguages->widget->getType() === 'select') {
                        $customValues = [];
                        foreach (WebLanguage::getWebLanguages() as $lang) {
                            $customValues[] = [
                                'value' => $lang->codicu,
                                'title' => $lang->name
                            ];
                        }
                        $columnLanguages->widget->setValuesFromArray($customValues);
                    }

                    // Keys Fields translates
                    $columnKeys = $this->views['EditSettingsTranslate']->columnForName('key');
                    if ($columnKeys && $columnKeys->widget->getType() === 'select') {
                        $customValues = [];
                        foreach (Settings::$fieldsTranslate as $field) {
                            $customValues[] = [
                                'value' => $field,
                                'title' => $field
                            ];
                        }
                        $columnKeys->widget->setValuesFromArray($customValues);
                    }

                    $where = [
                        new DataBaseWhere('modelid', 'webcreator'),
                        new DataBaseWhere('modelname', 'Settings')
                    ];
                    $view->loadData('', $where);
                    break;
            }
        };
    }
}