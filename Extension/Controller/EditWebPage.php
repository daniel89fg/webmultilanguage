<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Extension\Controller;

use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\WebLanguage;
use FacturaScripts\Dinamic\Model\WebPage;

class EditWebPage
{
    public function createViews() {
        return function() {
            $this->addEditListView('EditWebPageTranslate', 'WebTranslate', 'translations', 'fa fa-language');
            $this->setTabsPosition('top');
        };
    }

    protected function loadData() {
        return function($viewName, $view) {
            switch ($viewName) {
                case 'EditWebPageTranslate':
                    // Languages
                    $columnLanguages = $this->views['EditWebPageTranslate']->columnForName('language');
                    if ($columnLanguages && $columnLanguages->widget->getType() === 'select') {
                        $customValues = [];
                        foreach (WebLanguage::getWebLanguages() as $lang) {
                            $customValues[] = [
                                'value' => $lang->codicu,
                                'title' => $lang->name
                            ];
                        }
                        $columnLanguages->widget->setValuesFromArray($customValues);
                    }

                    // Keys Fields translates
                    $columnKeys = $this->views['EditWebPageTranslate']->columnForName('key');
                    if ($columnKeys && $columnKeys->widget->getType() === 'select') {
                        $customValues = [];
                        $page = new WebPage();
                        foreach ($page::$fieldsTranslate as $field) {
                            $customValues[] = [
                                'value' => $field,
                                'title' => $field
                            ];
                        }
                        $columnKeys->widget->setValuesFromArray($customValues);
                    }

                    $where = [
                        new DataBaseWhere('modelid', $this->request->get('code')),
                        new DataBaseWhere('modelname', 'WebPage')
                    ];
                    $view->loadData('', $where);
                    break;
            }
        };
    }
}