<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Extension\Controller;

use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\WebLanguage;

class EditContacto
{
    protected function loadData() {
        return function($viewName, $view) {
            switch ($viewName) {
                case 'EditContacto':
                    // Languages
                    $columnLanguages = $this->views['EditContacto']->columnForName('weblang');
                    if ($columnLanguages && $columnLanguages->widget->getType() === 'select') {
                        $customValues = [];
                        foreach (WebLanguage::getWebLanguages() as $lang) {
                            $customValues[] = [
                                'value' => $lang->codicu,
                                'title' => $lang->name
                            ];
                        }
                        $columnLanguages->widget->setValuesFromArray($customValues);
                    }

                    $where = [
                        new DataBaseWhere('idcontacto', $this->request->get('code'))
                    ];
                    $view->loadData('', $where);
                    break;
            }
        };
    }
}