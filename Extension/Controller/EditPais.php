<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Extension\Controller;

use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

class EditPais
{
    public function createViews() {
        return function() {
            $this->addListView('ListWebLanguage', 'WebLanguage', 'languages', 'fa fa-language');
            $this->setSettings('ListWebLanguage', 'codpais', false);
        };
    }

    protected function loadData() {
        return function($viewName, $view) {
            switch ($viewName) {
                case 'ListWebLanguage':
                    $where = [new DataBaseWhere('codpais', $this->request->get('code'))];
                    $view->loadData('', $where);
                    break;
            }
        };
    }
}