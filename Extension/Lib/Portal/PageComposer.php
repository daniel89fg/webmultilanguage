<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Extension\Lib\Portal;

use FacturaScripts\Dinamic\Model\WebTranslate;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Core\App\AppSettings;
use FacturaScripts\Dinamic\Model\WebLanguage;

class PageComposer
{
    public function getPagesDefaultAfter() {
        return function($homepage, $cookiespage, $privacypage, $termspage) {
            if (isset($_COOKIE['weblang'])) {
                $cookieLang = str_replace('-', '_', $_COOKIE['weblang']);
            } else {
                $langDefault = WebLanguage::getWebLanguageDefault();
                $cookieLang = $langDefault->codicu;
            }

            $transHomepage = new WebTranslate();
            $whereHomepage = [
                new DataBaseWhere('keytrans', 'permalink'),
                new DataBaseWhere('modelname', 'WebPage'),
                new DataBaseWhere('modelid', $homepage->idpage),
                new DataBaseWhere('codicu', $cookieLang)
            ];
            $transHomepage->loadFromCode('', $whereHomepage);
            $homepageUrl = AppSettings::get('webcreator', 'siteurl') . $transHomepage->valuetrans;
            
            $transCookiespage = new WebTranslate();
            $whereCookiespage = [
                new DataBaseWhere('keytrans', 'permalink'),
                new DataBaseWhere('modelname', 'WebPage'),
                new DataBaseWhere('modelid', $cookiespage->idpage),
                new DataBaseWhere('codicu', $cookieLang)
            ];
            $transCookiespage->loadFromCode('', $whereCookiespage);
            $cookiespageUrl = AppSettings::get('webcreator', 'siteurl') . $transCookiespage->valuetrans;

            $transPrivacypage = new WebTranslate();
            $wherePrivacypage = [
                new DataBaseWhere('keytrans', 'permalink'),
                new DataBaseWhere('modelname', 'WebPage'),
                new DataBaseWhere('modelid', $privacypage->idpage),
                new DataBaseWhere('codicu', $cookieLang)
            ];
            $transPrivacypage->loadFromCode('', $wherePrivacypage);
            $privacypageUrl = AppSettings::get('webcreator', 'siteurl') . $transPrivacypage->valuetrans;
            
            $transTermspage = new WebTranslate();
            $whereTermspage = [
                new DataBaseWhere('keytrans', 'permalink'),
                new DataBaseWhere('modelname', 'WebPage'),
                new DataBaseWhere('modelid', $termspage->idpage),
                new DataBaseWhere('codicu', $cookieLang)
            ];
            $transTermspage->loadFromCode('', $whereTermspage);
            $termspageUrl = AppSettings::get('webcreator', 'siteurl') . $transTermspage->valuetrans;

            return array(
                'homepage' => $homepageUrl,
                'cookiespage' => $cookiespageUrl,
                'privacypage' => $privacypageUrl,
                'termspage' => $termspageUrl
            );
        };
    }
}