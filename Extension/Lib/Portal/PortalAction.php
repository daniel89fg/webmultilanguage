<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Extension\Lib\Portal;

use FacturaScripts\Core\App\AppSettings;

class PortalAction
{
    public function execActionAfter() {
        return function($uri, $contact, $request) {
            
            /// change language web
            $newLang = $request->get('lang', '');
            if ($newLang) {
                $expire = \time() + \FS_COOKIES_EXPIRE;
                setcookie('weblang', $newLang, $expire, FS_ROUTE);

                header("Location: " . AppSettings::get('webcreator', 'siteurl') . $uri);
                exit();
            }
        };
    }
}