<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Extension\Model;

use FacturaScripts\Dinamic\Model\WebTranslate;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\WebLanguage;

class WebPage
{
    public function saveInsert() {
        return function() {
            $translate = new WebTranslate();
            $langs = WebLanguage::getWebLanguages();
            
            foreach (self::$fieldsTranslate as $field) {
                foreach ($langs as $lang) {
                    $translate->clear();

                    $where = [
                        new DataBaseWhere('codicu', $lang->codicu),
                        new DataBaseWhere('keytrans', $field),
                        new DataBaseWhere('modelname', $this->type),
                        new DataBaseWhere('modelid', $this->idpage)
                    ];

                    if (!$translate->loadFromCode('', $where)) {
                        if ($field == 'permalink') {
                            $link = explode('/', $this->permalink);
                            $this->$field = end($link);
                        }

                        $translate->codicu = $lang->codicu;
                        $translate->keytrans = $field;
                        $translate->valuetrans = $this->$field;
                        $translate->modelid = $this->idpage;
                        $translate->modelname = $this->type;
                        $translate->save();
                    }
                }
            }
        };
    }

    public function delete() {
        return function() {
            $where = [new DataBaseWhere('pageparent', $this->idpage)];

            $transModel = new WebTranslate();

            $where = [
                new DataBaseWhere('modelid', $this->idpage),
                new DataBaseWhere('modelname', 'WebPage')
            ];

            foreach ($transModel->all($where, [], 0, 0) as $trans) {
                $trans->delete();
            }
        };
    }
}