<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Model;

use FacturaScripts\Core\Model\Base;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of Language
 *
 * @author Athos Online <info@athosonline.com>
 */
class WebLanguage extends Base\ModelClass
{
    use Base\ModelTrait;
    
    /**
     *
     * @var string
     */
    public $codicu;

    /**
     *
     * @var string
     */
    public $codpais;

    /**
     *
     * @var int
     */
    public $default;
    
    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var int
     */
    public $web;
    
    public static function primaryColumn()
    {
        return 'codicu';
    }

    public static function tableName()
    {
        return 'weblanguages';
    }

    /**
     * Returns the url where to see / modify the data.
     *
     * @param string $type
     * @param string $list
     *
     * @return string
     */
    public function url(string $type = 'auto', string $list = 'ListPais?activetab=List')
    {
        return parent::url($type, $list);
    }

    public function test()
    {
        $utils = $this->toolBox()->utils();
        $this->codicu = str_replace([' ', '-'], '_', $utils->noHtml($this->codicu));
        return parent::test();
    }

    public static function getWebLanguages()
    {
        $langs = new WebLanguage();
        $where = [new DataBaseWhere('web', 1)];
        return $langs->all($where, [], 0, 0);
    }

    public static function getWebLanguageDefault()
    {
        $lang = new WebLanguage();
        $where = [new DataBaseWhere('default', 1)];
        $lang->loadFromCode('', $where);
        return $lang;
    }

    public static function getWebLangFile()
    {
        if (isset($_COOKIE['weblang'])) {
            return str_replace('-', '_', $_COOKIE['weblang']);
        } else {
            $lang = self::getWebLanguageDefault();
            return $lang->codicu;
        }
    }
}