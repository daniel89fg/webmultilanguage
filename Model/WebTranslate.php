<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Model;

use FacturaScripts\Core\Model\Base;
use FacturaScripts\Dinamic\Model\WebLanguage;
use FacturaScripts\Dinamic\Model\WebPage;
use FacturaScripts\Plugins\WebMultilanguage\Lib\Portal\PermalinkTranslateTrait;
use FacturaScripts\Dinamic\Lib\Portal\UpdateRoutes;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of WebTranslate
 *
 * @author Athos Online <info@athosonline.com>
 */
class WebTranslate extends Base\ModelOnChangeClass
{
    use Base\ModelTrait;
    use PermalinkTranslateTrait;
    
    /**
     *
     * @var string
     */
    public $codicu;

    /**
     *
     * @var serial
     */
    public $idtrans;
    
    /**
     *
     * @var string
     */
    public $keytrans;

    /**
     *
     * @var string
     */
    public $valuetrans;

    /**
     *
     * @var string
     */
    public $modelid;

    /**
     *
     * @var string
     */
    public $modelname;

    protected function setPreviousData(array $fields = [])
    {
        $more = ['keytrans', 'valuetrans', 'modelid', 'modelname', 'codicu'];
        parent::setPreviousData(array_merge($more, $fields));
    }
    
    public static function primaryColumn()
    {
        return 'idtrans';
    }

    public static function tableName()
    {
        return 'webtranslates';
    }

    /**
     *
     * @return string
     */
    public function install()
    {
        /// needed dependencies
        new WebLanguage();

        return parent::install();
    }

    public function save()
    {
        $updateRoutes = false;
        if ($this->keytrans === 'permalink' && !empty($this->modelname) && !empty($this->modelid)) {
            $this->valuetrans = $this->checkPermalink($this);
            $updateRoutes = true;
        }

        if (parent::save()) {
            if ($updateRoutes) {
                $this->refreshPermalinkSons($this->modelid, $this->codicu);
                $routes = new UpdateRoutes();
                $routes->setRoutes();
            }
            return true;
        }

        return false;
    }

    public function delete()
    {      
        if (parent::delete()) {
            if ($this->keytrans == 'permalink' && $this->modelname == 'WebPage' && !empty($this->modelid)) {
                $this->refreshPermalinkSons($this->modelid, $this->codicu);
                $routes = new UpdateRoutes();
                $routes->setRoutes();
            }
            return true;
        }

        return false;
    }

    protected function refreshPermalinkSons($idpage, $codicu)
    {
        $pageModel = new WebPage();
        $where = [new DataBaseWhere('pageparent', $idpage)];

        foreach ($pageModel->all($where, [], 0, 0) as $page) {
            $transModel = new WebTranslate();

            $where = [
                new DataBaseWhere('keytrans', 'permalink'),
                new DataBaseWhere('modelid', $page->idpage),
                new DataBaseWhere('codicu', $codicu),
                new DataBaseWhere('modelname', 'WebPage')
            ];
    
            foreach ($transModel->all($where, [], 0, 0) as $trans) {
                $url = explode('/', $trans->valuetrans);
                $trans->valuetrans = end($url);
                $trans->save();
            }
        }
    }
}