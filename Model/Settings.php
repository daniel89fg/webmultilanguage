<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Model;

use FacturaScripts\Core\Model\Settings as parentModel;

/**
 * Description of Settings
 *
 * @author Athos Online <info@athosonline.com>
 */
class Settings extends parentModel
{
    public static $fieldsTranslate = [];
}