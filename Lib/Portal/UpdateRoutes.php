<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Portal;

use FacturaScripts\Core\App\AppRouter;
use FacturaScripts\Plugins\WebMultilanguage\Model\WebTranslate;
use FacturaScripts\Dinamic\Model\WebPage;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Core\App\AppSettings;
use FacturaScripts\Core\Base\ExtensionsTrait;

class UpdateRoutes
{
    use ExtensionsTrait;

    public function setRoutes()
    {
        $this->pipe('setRoutesBefore');

        /// we must clear FacturaScripts custom routes in order to set the new ones.
        $appRouter = new AppRouter();
        $appRouter->clear();

        /// we need the homepage
        $homePage = new WebPage();
        $homePage->loadFromCode(AppSettings::get('webcreator', 'homepage'));
        $customController = empty($homePage->customcontroller) ? 'PortalHome' : $homePage->customcontroller;
        $appRouter->setRoute('/', $customController, $homePage->idpage, false);

        $transModel = new WebTranslate();

        $where = [
            new DataBaseWhere('keytrans', 'permalink'),
            new DataBaseWhere('modelid', null, 'IS NOT'),
            new DataBaseWhere('modelname', null, 'IS NOT')
        ];

        foreach ($transModel->all($where, [], 0, 0) as $trans) {
            $page = new WebPage();
            $page->loadFromCode($trans->modelid);

            $customController = empty($page->customcontroller) ? 'PortalHome' : $page->customcontroller;
            $appRouter->setRoute($trans->valuetrans, $customController, $trans->modelid, false);
        }

        $this->pipe('setRoutesAfter');
    }
}