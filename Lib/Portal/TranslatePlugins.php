<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Portal;

use FacturaScripts\Plugins\WebMultilanguage\Model\WebTranslate;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\WebLanguage;
use FacturaScripts\Dinamic\Model\Settings;

class TranslatePlugins
{
    public static function checkPlugin($pluginName = null)
    {
        $path = \FS_FOLDER . '/Plugins/';
        
        if (is_null($pluginName)) {
            $dir = opendir($path);
            while ($current = readdir($dir)) {
                if( $current != "." && $current != ".." && is_dir($path.$current)) {
                    $dirPlugin = $path.$current.'/';
                    self::checkModel($dirPlugin);
                }
            }
        } else {
            $dirPlugin = $path.$pluginName.'/';
            self::checkModel($dirPlugin);
        }
    }

    private static function checkModel($dirPlugin)
    {
        $dirPlugin .= 'Model/';
        if (file_exists($dirPlugin)) {
            $dir = opendir($dirPlugin);
            while ($current = readdir($dir)) {
                if( $current != "." && $current != ".." && is_file($dirPlugin.$current)) {
                    $modelName = str_replace('.php', '', $current);
                    $modelClass = '\\FacturaScripts\\Dinamic\\Model\\' . $modelName;
                    if (\class_exists($modelClass)) {
                        $model = new $modelClass();
                        
                        if (isset($model::$fieldsTranslate)) {
                            self::translateModel($model, $modelName);
                        }
                    }
                }
            }
        }
    }

    private static function translateModel($model, $modelName)
    {
        $translate = new WebTranslate();
        $langs = WebLanguage::getWebLanguages();
        foreach ($model->all([], [], 0, 0) as $data) {
            $id = $data::primaryColumn();
            
            foreach ($data::$fieldsTranslate as $field) {
                foreach ($langs as $lang) {
                    $translate->clear();

                    $where = [
                        new DataBaseWhere('codicu', $lang->codicu),
                        new DataBaseWhere('keytrans', $field),
                        new DataBaseWhere('modelname', $modelName),
                        new DataBaseWhere('modelid', $data->$id)
                    ];

                    if (!$translate->loadFromCode('', $where)) {
                        if ($field == 'permalink') {
                            $link = explode('/', $data->$field);
                            $data->$field = end($link);
                        }

                        $translate->codicu = $lang->codicu;
                        $translate->keytrans = $field;
                        $translate->valuetrans = $data->$field;
                        $translate->modelid = $data->$id;
                        $translate->modelname = $modelName;
                        $translate->save();
                    }
                }
            }
        }
    }

    public static function translateSettings()
    {
        $setting = new Settings();
        $langs = WebLanguage::getWebLanguages();
        $translate = new WebTranslate();
        
        foreach (Settings::$fieldsTranslate as $field) {
            $setting->clear();

            foreach ($langs as $lang) {
                $translate->clear();

                $where = [
                    new DataBaseWhere('codicu', $lang->codicu),
                    new DataBaseWhere('keytrans', $field),
                    new DataBaseWhere('modelname', 'Settings'),
                    new DataBaseWhere('modelid', 'webcreator')
                ];

                if (!$translate->loadFromCode('', $where)) {
                    $value = $setting->get('webcreator')->$field;
                    if (!empty($value)) {
                        $translate->codicu = $lang->codicu;
                        $translate->keytrans = $field;
                        $translate->valuetrans = $value ;
                        $translate->modelid = 'webcreator';
                        $translate->modelname = 'Settings';
                        $translate->save();
                    }
                }
            }
        }
    }
}