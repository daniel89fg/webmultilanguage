<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Portal;

use FacturaScripts\Plugins\WebCreator\Lib\Portal\WebPageData as parentController;
use FacturaScripts\Dinamic\Model\WebTranslate;
use FacturaScripts\Dinamic\Model\WebPage;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Core\App\AppSettings;
use FacturaScripts\Dinamic\Model\WebLanguage;

class WebPageData extends parentController
{
    protected function getPerfectPage($webPage)
    {
        $page = new Webpage();
        $transPage = new WebTranslate();

        $this->pipe('getPerfectPageBefore');

        if ($this->uri === '/') {
            $page->loadFromCode(AppSettings::get('webcreator', 'homepage'));
            $this->pipe('getPerfectPageAfter');
            return $page;
        } else {
            $where = [
                new DataBaseWhere('keytrans', 'permalink'),
                new DataBaseWhere('valuetrans', $this->uri),
            ];
    
            if ($transPage->loadFromCode('', $where)) {
                $page->loadFromCode($transPage->modelid);
                $this->pipe('getPerfectPageAfter');
                return $page;
            }
        }        

        $this->pipe('getPerfectPageAfter');
        return null;
    }

    protected function setLang($webPage)
    {
        $this->pipe('setLangBefore');

        $weblang = null;
        $cookieLang = null;
        $defaultLang = WebLanguage::getWebLanguageDefault();
        $webPage->canonicalUrl = AppSettings::get('webcreator', 'siteurl');

        if (isset($_COOKIE['weblang'])) {
            $cookieLang = str_replace('-', '_', $_COOKIE['weblang']);
        }

        if (isset($webPage->idpage)) {
            $transPageModel = new WebTranslate();
            $where = [
                new DataBaseWhere('modelid', $webPage->idpage),
                new DataBaseWhere('modelname', $webPage->type)
            ];
            $transPage = $transPageModel->all($where, [], 0, 0);

            foreach ($transPage as $trans) {
                if (is_null($weblang) && $trans->keytrans === 'permalink' && $this->uri == $trans->valuetrans) {
                    $weblang = $trans->codicu;
                    $webPage->canonicalUrl .= $trans->valuetrans;
                } else if (is_null($weblang) && $trans->keytrans === 'permalink' && $this->uri === '/'
                && $trans->modelid == AppSettings::get('webcreator', 'homepage')
                && $cookieLang == $trans->codicu) {
                    $weblang = $trans->codicu;
                } else if (is_null($weblang) && $trans->keytrans === 'permalink' && $this->uri === '/'
                && $trans->modelid == AppSettings::get('webcreator', 'homepage')) {
                    if (!is_null($cookieLang)) {
                        $weblang = $cookieLang;
                    } else {
                        $weblang = $defaultLang->codicu;
                    }
                }
            }

            $webPage->filelang = $weblang;
            
            $webPage->languages = [];
            foreach ($transPage as $trans) {
                if ($trans->keytrans === 'title' && isset($webPage->filelang) && $webPage->filelang == $trans->codicu) {
                    $webPage->title = $trans->valuetrans;
                }

                if ($trans->keytrans === 'description' && isset($webPage->filelang) && $webPage->filelang == $trans->codicu) {
                    $webPage->description = $trans->valuetrans;
                }

                if (!array_key_exists($trans->codicu, $webPage->languages)) {                
                    foreach ($transPage as $trans2) {
                        if ($trans->codicu === $trans2->codicu) {
                            $pageLanguages['weblang'] = str_replace('_', '-', $trans2->codicu);
                            $pageLanguages['filelang'] = $trans2->codicu;
                            $pageLanguages['default'] = false;
    
                            if ($trans2->keytrans === 'title') {
                                $pageLanguages['title'] = $trans2->valuetrans;
                            }
            
                            if ($trans2->keytrans === 'description') {
                                $pageLanguages['description'] = $trans2->valuetrans;
                            }
            
                            if ($trans2->keytrans === 'permalink') {
                                $pageLanguages['permalink'] = AppSettings::get('webcreator', 'siteurl') . $trans2->valuetrans;
                            }
            
                            if (AppSettings::get('webcreator', 'homepage') == $trans2->modelid) {
                                $pageLanguages['default'] = true;
                                $pageLanguages['permalink'] = AppSettings::get('webcreator', 'siteurl');
                            }
                        }
                    }

                    $webPage->languages[$trans->codicu] = $pageLanguages;
                }
            }                    
        }

        if (!isset($webPage->filelang)) {
            if (is_null($cookieLang) && !empty($this->contact)) {
                $webPage->filelang = $this->contact->codicu;
            } else if (!is_null($cookieLang)) {
                $webPage->filelang = $cookieLang;
            } else if (is_null($cookieLang)) {
                $webPage->filelang = $defaultLang->codicu;
            }
        }

        $webPage->weblang = str_replace('_', '-', $webPage->filelang);
        $expire = \time() + \FS_COOKIES_EXPIRE;
        setcookie('weblang', $webPage->weblang, $expire, FS_ROUTE);
        
        $this->pipe('setLangAfter');

        return $webPage;
    }
}