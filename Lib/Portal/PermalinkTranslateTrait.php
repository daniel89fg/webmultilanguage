<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Portal;

use FacturaScripts\Dinamic\Model\WebPage;
use FacturaScripts\Plugins\WebMultilanguage\Model\WebTranslate;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Lib\Portal\GetRoutes;

trait PermalinkTranslateTrait
{
    public $translateOrig;
    public $permalinkFinal;

    /**
     * Returns the url of the page
     *
     * @param WebTranslate $translate
     *
     * @return string
     */
    public function checkPermalink($translate)
    {
        $utils = $this->toolBox()->utils();
        $permalink = $utils->noHtml($translate->valuetrans);

        $changes = ['/à/' => 'a', '/á/' => 'a', '/â/' => 'a', '/ã/' => 'a', '/ä/' => 'a',
            '/å/' => 'a', '/æ/' => 'ae', '/ç/' => 'c', '/è/' => 'e', '/é/' => 'e', '/ê/' => 'e',
            '/ë/' => 'e', '/ì/' => 'i', '/í/' => 'i', '/î/' => 'i', '/ï/' => 'i', '/ð/' => 'd',
            '/ñ/' => 'n', '/ò/' => 'o', '/ó/' => 'o', '/ô/' => 'o', '/õ/' => 'o', '/ö/' => 'o',
            '/ő/' => 'o', '/ø/' => 'o', '/ù/' => 'u', '/ú/' => 'u', '/û/' => 'u', '/ü/' => 'u',
            '/ű/' => 'u', '/ý/' => 'y', '/þ/' => 'th', '/ÿ/' => 'y', '/&quot;/' => '-'
        ];

        $permalink = \preg_replace(\array_keys($changes), $changes, \strtolower($permalink));
        $permalink = \preg_replace('/[^a-z0-9.]/i', '-', $permalink);
        $permalink = \preg_replace('/-+/', '-', $permalink);

        if (\substr($permalink, 0, 1) == '-' || substr($permalink, 0, 1) == '/') {
            $permalink = \substr($permalink, 1);
        }

        if (\substr($permalink, -1) == '-' || substr($permalink, -1) == '/') {
            $permalink = \substr($permalink, 0, -1);
        }

        $webpage = new WebPage();
        $webpage->loadFromCode($translate->modelid);
        
        if ($webpage->type !== 'WebPage') {
            $permalink_plugin = new WebTranslate();
            $where = [
                new DataBaseWhere('keytrans', 'permalink_' . strtolower($webpage->type)),
                new DataBaseWhere('modelid', 'webcreator'),
                new DataBaseWhere('modelname', 'Settings'),
                new DataBaseWhere('codicu', $translate->codicu)
            ];
            $permalink_plugin->loadFromCode('', $where);
            $permalink = $permalink_plugin->valuetrans . '/' . $permalink;
        }
        
        $this->translateOrig = $translate;
        
        return $this->parentpermalink($permalink, $webpage);
    }

    /**
     * Create the page url
     *
     * @param string $permalink
     * @param WebPage $webpage
     *
     * @return string
     */
    public function parentpermalink($permalink, $webpage)
    {
        if (!empty($webpage->pageparent)) {
            $parent = new WebPage();
            $parent->loadFromCode($webpage->pageparent);
            
            $translate = new WebTranslate();
            $where = [
                new DataBaseWhere('keytrans', 'permalink'),
                new DataBaseWhere('modelid', $parent->idpage),
                new DataBaseWhere('modelname', 'WebPage'),
                new DataBaseWhere('codicu', $this->translateOrig->codicu)
            ];
            $translate->loadFromCode('', $where);
            
            if (isset($translate->idtrans)) {
                $parentPermalink = \substr($translate->valuetrans, 1);
            } else {
                $parentPermalink = \substr($parent->permalink, 1);
            }
            
            $permalink = $parentPermalink . '/' . $permalink;
            $this->parentpermalink($permalink, $parent);
        }

        $this->permalinkFinal = '/' . $permalink;
        return $this->findPermalink($this->permalinkFinal);
    }

    /**
     * Search if there are more equal urls
     *
     * @param string $permalink
     *
     * @return string
     */
    public function findPermalink($permalink, $num = 1)
    {
        foreach (GetRoutes::getRoutes() as $key => $value) {
            if ($key === $permalink) {
                $translate = new WebTranslate();
                $where = [
                    new DataBaseWhere('keytrans', 'permalink'),
                    new DataBaseWhere('valuetrans', $permalink)
                ];
                $translate->loadFromCode('', $where);
                
                if (isset($translate->idtrans) && $translate->idtrans != $this->translateOrig->idtrans) {
                    $permalink .= '-2';
                }
            }
        }
        
        return $permalink;
    }
}