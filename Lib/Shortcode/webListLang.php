<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Shortcode;

use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;
use FacturaScripts\Dinamic\Model\WebLanguage;
use FacturaScripts\Dinamic\Model\WebTranslate;
use FacturaScripts\Core\App\AppSettings;
use FacturaScripts\Core\Base\ToolBox;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Shortcode of webListLang
 * Shows the list of languages available on the web.
 *
 * @author Athos Online <info@athosonline.com>
 */
class webListLang extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webListLang(.*?)\][\r\n|\n]*(.*?)[\r\n|\n]*\[\/webListLang\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }

        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $actual_link = str_replace(AppSettings::get('webcreator', 'siteurl'), '', $actual_link);
        
        $translate = new WebTranslate();
        $where = [
            new DataBaseWhere('modelid', null, 'IS NOT'),
            new DataBaseWhere('modelname', null, 'IS NOT'),
            new DataBaseWhere('keytrans', 'permalink'),
            new DataBaseWhere('valuetrans', $actual_link)
        ];
        
        if ($translate->loadFromCode('', $where)) {
            $transPageModel = new WebTranslate();
            $where = [
                new DataBaseWhere('modelid', $translate->modelid),
                new DataBaseWhere('modelname', null, 'IS NOT'),
                new DataBaseWhere('keytrans', 'permalink')
            ];
            $transPage = $transPageModel->all($where, [], 0, 0);
        }
        
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            $class = isset($params['class']) ? $params['class'] : '';
            $id = isset($params['id']) ? $params['id'] : '';
            $flag = (isset($params['flag']) && $params['flag'] == 'yes') ? true : false;
            $width = isset($params['width']) ? $params['width'] : 'auto';
            $height = isset($params['height']) ? $params['height'] : 'auto';
            $codicu = (isset($params['codicu']) && $params['codicu'] == 'yes') ? true : false;
            $name = (isset($params['name']) && $params['name'] == 'yes') ? true : false;
            $translate = (isset($params['translate']) && $params['translate'] == 'yes') ? true : false;
            $inline = (isset($params['inline']) && $params['inline'] == 'yes') ? 'list-inline' : '';
            $inlineItem = ($inline != '') ? 'list-inline-item' : '';
            $dropdown = (isset($params['dropdown']) && $params['dropdown'] == 'yes') ? true : false;

            if (isset($_COOKIE['weblang'])) {
                $cookieLang = str_replace('-', '_', $_COOKIE['weblang']);
            } else {
                $langDefault = WebLanguage::getWebLanguageDefault();
                $cookieLang = $langDefault->codicu;
            }  
            
            $langs = WebLanguage::getWebLanguages();
            $cont = 1;

            if ($dropdown) {
                $txt = ToolBox::i18n()->customTrans($cookieLang, 'languages');

                if (isset($shorts[2][$x]) && $shorts[2][$x] != '') {
                    $txt = $shorts[2][$x];
                }

                $html = '
                    <div id="'.$id.'" class="list-lang dropdown ' . $class . '">
                        <a class="dropdown-toggle text-decoration-none" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ' . $txt . '
                        </a>
              
                    <div class="dropdown-menu text-center" aria-labelledby="dropdownMenuLink">';
            } else {
                $html = '<ul id="'.$id.'" class="list-lang list-unstyled ' . $class . ' ' . $inline . '">';
            }

            foreach ($langs as $lang) {
                $link = AppSettings::get('webcreator', 'siteurl');
                $active = ($lang->codicu == $cookieLang) ? 'active' : '';

                if ($actual_link !== '/' && isset($transPage)) {
                    foreach ($transPage as $trans) {
                        if ($trans->codicu == $lang->codicu) {
                            $link .= $trans->valuetrans;
                        }
                    }
                } else {
                    $link .= $actual_link . '?lang=' . $lang->codicu;
                }

                if ($dropdown) {
                    $html .= '<a href="' . $link . '" class="dropdown-item ' . $active . ' list-lang-link ' . $inlineItem . '">';
                } else {
                    $html .= '<li class="' . $active . ' list-lang-item-' . $cont . ' ' . $inlineItem . '">';
                    $html .= '<a href="' . $link . '" class="list-lang-link">';
                }

                if ($flag) {
                    $url = AppSettings::get('webcreator', 'siteurl') . '/MyFiles/Public/' . $lang->flag;
                    $html .= '<img src="'.$url.'" height="'.$height.'" width="'.$width.'" />';
                }

                if ($codicu) {
                    $html .= $lang->codicu;
                }

                if ($name) {
                    if ($translate) {
                        $html .= ToolBox::i18n()->customTrans($cookieLang, $lang->name);
                    } else {
                        $html .= $lang->name;
                    }
                }

                if ($dropdown) {
                    $html .= '</a>';
                } else {
                    $html .= '</a>';
                    $html .= '</li>';
                }
                
                $cont++;
            }

            if ($dropdown) {
                $html .= '</div>';
            } else {
                $html .= '</ul>';
            }

            $content = str_replace($shorts[0][$x], $html, $content);
        }

        return $content;
    }
}