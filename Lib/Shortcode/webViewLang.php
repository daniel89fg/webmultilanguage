<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Shortcode;

use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;
use FacturaScripts\Dinamic\Model\WebLanguage;

/**
 * Shortcode of webViewLang
 * Displays content only in selected languages.
 *
 * @author Athos Online <info@athosonline.com>
 */
class webViewLang extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webViewLang(.*?)\][\r\n|\n]*(.*?)[\r\n|\n]*\[\/webViewLang\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }

        if (isset($_COOKIE['weblang'])) {
            $weblang = str_replace('-', '_', $_COOKIE['weblang']);
        } else {
            $langDefault = WebLanguage::getWebLanguageDefault();
            $weblang = $langDefault->codicu;
        }

        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            $displayLang = isset($params['lang']) ? explode(',', $params['lang']) : [];

            $html = '';
            $viewContent = false;

            foreach ($displayLang as $lang) {
                if ($lang == $weblang) {
                    $viewContent = true;
                }
            }

            if ($viewContent) {
                $html = $shorts[2][$x];
            }

            $content = str_replace($shorts[0][$x], $html, $content);
        }

        return $content;
    }
}