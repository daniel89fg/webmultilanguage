<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Shortcode;

use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;
use FacturaScripts\Dinamic\Model\WebLanguage;
use FacturaScripts\Dinamic\Model\WebTranslate;
use FacturaScripts\Core\App\AppSettings;
use FacturaScripts\Core\Base\ToolBox;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Shortcode of webActualLang
 * Returns the current language of the web.
 *
 * @author Athos Online <info@athosonline.com>
 */
class webActualLang extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webActualLang(.*?)\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }

        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);

            $flag = (isset($params['flag']) && $params['flag'] == 'yes') ? true : false;
            $width = isset($params['width']) ? $params['width'] : 'auto';
            $height = isset($params['height']) ? $params['height'] : 'auto';
            $codicu = (isset($params['codicu']) && $params['codicu'] == 'yes') ? true : false;
            $name = (isset($params['name']) && $params['name'] == 'yes') ? true : false;
            $translate = (isset($params['translate']) && $params['translate'] == 'yes') ? true : false;
            
            if (isset($_COOKIE['weblang'])) {
                $cookieLang = str_replace('-', '_', $_COOKIE['weblang']);
            } else {
                $langDefault = WebLanguage::getWebLanguageDefault();
                $cookieLang = $langDefault->codicu;
            }
            
            $weblang = new WebLanguage();
            $weblang->loadFromCode($cookieLang);

            $html = '';

            if ($flag) {
                $url = AppSettings::get('webcreator', 'siteurl') . '/MyFiles/Public/' . $weblang->flag;
                $html .= '<img src="'.$url.'" height="'.$height.'" width="'.$width.'" />';
            }

            if ($codicu) {
                $html .= $weblang->codicu;
            }

            if ($name) {
                if ($translate) {
                    $html .= ToolBox::i18n()->customTrans($cookieLang, $weblang->name);
                } else {
                    $html .= $weblang->name;
                }
            }

            $content = str_replace($shorts[0][$x], $html, $content);
        }

        return $content;
    }
}