<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Widget;

use FacturaScripts\Core\Lib\Widget\WidgetTextarea;
use FacturaScripts\Core\Base\ToolBox;

/**
 * Description of WidgetPermalink
 *
 * @author Athos Online <info@athosonline.com>
 */
class WidgetPermalink extends WidgetTextarea
{
    protected function inputHtml($type = 'text', $extraClass = '')
    {
        $class = $this->combineClasses($this->css('form-control'), $this->class, $extraClass);
        
        if (substr($this->value, 0, 1) === '/') {
            $urlArray = explode('/', $this->value);
            $url = $this->toolBox()->appSettings()->get('webcreator', 'siteurl') . implode("/", $urlArray);

            $html = '<textarea rows="' . $this->rows . '" name="' . $this->fieldname . '" class="' . $class . '"'
                . $this->inputHtmlExtraParams() . '>' . end($urlArray) . '</textarea>';
            $html .= '<small><a target="_blank" href="' . $url . '">' . $url . '</a></small>';
        } else {
            $html = '<textarea rows="' . $this->rows . '" name="' . $this->fieldname . '" class="' . $class . '"'
                . $this->inputHtmlExtraParams() . '>' . $this->value . '</textarea>';
        }
        
        return $html;
    }

    private function toolBox()
    {
        return new ToolBox();
    }
}