<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Widget;

use FacturaScripts\Core\Lib\Widget\BaseWidget;
use FacturaScripts\Core\Base\ToolBox;

/**
 * Description of WidgetFlag
 *
 * @author Athos Online <info@athosonline.com>
 */
class WidgetFlag extends BaseWidget
{
    /**
     *
     * @var int
     */
    public $width;

    /**
     *
     * @var int
     */
    public $height;

    public function __construct($data)
    {
        parent::__construct($data);
        $this->width = $data['width'] ?? 'auto';
        $this->height = $data['height'] ?? 'auto';
    }

    /**
     * 
     * @param string $type
     * @param string $extraClass
     *
     * @return string
     */
    protected function show()
    {
        $url = '';

        if (!is_null($this->value)) {
            $url = $this->toolBox()->appSettings()->get('webcreator', 'siteurl') . '/MyFiles/Public/' . $this->value;
        }
        
        return '<img src="'.$url.'" height="'.$this->height.'" width="'.$this->width.'" />';
    }

    private function toolBox()
    {
        return new ToolBox();
    }
}