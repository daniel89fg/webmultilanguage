<?php

namespace FacturaScripts\Plugins\WebMultilanguage\Lib\Widget;

use FacturaScripts\Core\Lib\Widget\WidgetFile;

/**
 * Description of WidgetFlag
 *
 * @author Athos Online <info@athosonline.com>
 */
class WidgetFileFlag extends WidgetFile
{
    /**
     * 
     * @param string $type
     * @param string $extraClass
     *
     * @return string
     */
    protected function inputHtml($type = 'file', $extraClass = '')
    {
        $class = empty($extraClass) ? $this->css('form-control-file') : $this->css('form-control-file') . ' ' . $extraClass;
        
        if (empty($this->value)) {
            return '<input type="' . $type . '" name="' . $this->fieldname . '" value="' . $this->value
            . '" class="' . $class . '"' . $this->inputHtmlExtraParams() . '/>';
        } else {
            $url = FS_ROUTE . '/MyFiles/Public/' . $this->value;

            $html = '<div class="d-flex flex-row"><img class="mr-2" src="'.$url.'" height="30" width="auto"/><input type="' . $type . '" name="' . $this->fieldname . '" value="' . $this->value
            . '" class="' . $class . '"' . $this->inputHtmlExtraParams() . '/></div>';

            return $html;
        }
    }
}