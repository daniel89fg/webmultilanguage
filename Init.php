<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMultilanguage;

use FacturaScripts\Core\Base\InitClass;
use FacturaScripts\Dinamic\Model\WebLanguage;
use FacturaScripts\Dinamic\Model\Contacto;
use FacturaScripts\Plugins\WebCreator\Lib\Shortcode\Shortcode;
use FacturaScripts\Dinamic\Lib\Portal\TranslatePlugins;

/**
 * Description of Init
 *
 * @author Athos Online <info@athosonline.com>
 */
class Init extends InitClass
{
    public function init()
    {
        $this->loadExtension(new Extension\Lib\Portal\PortalAction());
        $this->loadExtension(new Extension\Lib\Portal\PageComposer());
        $this->loadExtension(new Extension\Controller\ListPais());
        $this->loadExtension(new Extension\Controller\EditPais());
        $this->loadExtension(new Extension\Controller\EditWebPage());
        $this->loadExtension(new Extension\Controller\WebCreator());
        $this->loadExtension(new Extension\Controller\WebCreator());
        $this->loadExtension(new Extension\Controller\EditContacto());
        $this->loadExtension(new Extension\Model\WebPage());

        Shortcode::addCode('listlang', 'webListLang');
        Shortcode::addCode('actuallang', 'webActualLang');
        Shortcode::addCode('viewlang', 'webViewLang');
    }

    public function update()
    {
        new WebLanguage();
        $this->setImageFlags();
        $this->setLangContacts();
        TranslatePlugins::checkPlugin();
    }

    public function setImageFlags()
    {
        $pathMyFiles = FS_FOLDER . '/Plugins/WebMultilanguage/MyFiles/Public/';
        foreach(scandir($pathMyFiles) as $file) {
            if ($file != '.' && $file != '..') {
                $filename = $pathMyFiles . $file;
                $findFile = FS_FOLDER . '/MyFiles/Public/' . $file;
                if (!file_exists($findFile)) {
                    copy($filename, $findFile);
                }
            }
        }
    }

    public function setLangContacts()
    {
        $contacts = new Contacto();
        $lang = WebLanguage::getWebLanguageDefault();
        
        foreach ($contacts->all([], [], 0, 0) as $contact) {
            if (is_null($contact->codicu)) {
                $contact->codicu = $lang->codicu;
                $contact->save();
            }
        }
    }
}